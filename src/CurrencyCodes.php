<?php

namespace BestChange;

use BestChange\Exception\BestChangeException;

class CurrencyCodes
{
    private $data = [];

    /**
     * Currencies constructor.
     * @param $data
     * @throws BestChangeException
     */
    public function __construct($data)
    {
        $data = explode("\n", $data);
        foreach ($data as $row) {
            $row = iconv('CP1251', 'UTF-8', $row);
            $data = explode(';', $row);
            $this->data[$data[0]] = [
                'id' => (int)$data[0],
                'code' => $data[1],
            ];
        }
    }

    public function get()
    {
        return $this->data;
    }

    public function getByID($id)
    {
        return empty($this->data[$id]) ? false : $this->data[$id];
    }
}